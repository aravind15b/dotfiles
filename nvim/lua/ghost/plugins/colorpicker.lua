-- import comment plugin safely
local setup, colorpicker = pcall(require, "color-picker")
if not setup then
	return
end

-- enable comment
colorpicker.setup()
