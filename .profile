#export ZDOTDIR="$HOME/.config/zsh"
export EDITOR=/usr/bin/vim
export PATH=$PATH:/home/ghost/.local/bin
export ANDROID_SDK_ROOT=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/emulator
export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
